# Asteroids Clone

This project was created to make a spinoff clone game of the original Asteroids Arcade Game. 

The goal of the project was to learn Unreal Engine's C++ paradigms with a simple project.  
  
To vary gameplay from the original arcade game I also decided to implement a two weapon mechanic and changed the Asteroid splitting logic. In the original game, asteroids only explode/split when they are shot, but in this game, asteroids will destroy other asteroids of equal or less size.

# Project Specification

The game was created in one week using Unreal Engine 5 and C++.
