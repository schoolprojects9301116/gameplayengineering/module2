// Fill out your copyright notice in the Description page of Project Settings.

#include "AsteroidsGameMode.h"
#include "AsteroidsCharacter.h"
#include "AsteroidEnemy.h"
#include "AsteroidsGameState.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerStart.h"

AAsteroidsGameMode::AAsteroidsGameMode()
{
	DefaultPawnClass = AAsteroidsCharacter::StaticClass();
}

void AAsteroidsGameMode::PlayerDied(APawn* player)
{
	AAsteroidsGameState* gameState = GetGameState<AAsteroidsGameState>();
	gameState->NumPlayerLives--;
	if (gameState->NumPlayerLives > 0)
	{
		auto world = GetWorld();
		TArray<AActor*> foundActors;
		UGameplayStatics::GetAllActorsOfClass(world, APlayerStart::StaticClass(), foundActors);
		auto playerStart = Cast<APlayerStart>(foundActors[0]);
		auto playerController = player->GetController();
		playerController->UnPossess();
		auto playerClass = player->GetClass();

		auto newPlayer = world->SpawnActor<AActor>(playerClass, playerStart->GetActorLocation(), playerStart->GetActorRotation());

		if (newPlayer)
		{
			auto playerPawn = Cast<APawn>(newPlayer);
			if (playerPawn)
			{
				playerController->Possess(playerPawn);
			}
		}
	}
}



void AAsteroidsGameMode::StartPlay()
{
	AAsteroidsGameState* gameState = GetGameState<AAsteroidsGameState>();
	gameState->NumPlayerLives = gameState->StartingPlayerLives;
	gameState->CurrentWave = 1;
	Super::StartPlay();
}

void AAsteroidsGameMode::ModifyAsteroidCount(int modifier)
{

	AAsteroidsGameState* gameState = GetGameState<AAsteroidsGameState>();
	gameState->AsteroidCount += modifier;
}

void AAsteroidsGameMode::ModifyScore(int modifier)
{
	AAsteroidsGameState* gameState = GetGameState<AAsteroidsGameState>();
	gameState->PlayerScore += modifier;
}