// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameCharacter.generated.h"

UCLASS()
class MODULE2_API AGameCharacter : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AGameCharacter();

	void RotateLR(float inputValue);
	void MoveForward(float inputValue);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere)
	USceneComponent* _rootSceneComponent;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* _CharacterMesh;

	UPROPERTY(EditAnywhere)
	float _rotationSpeed;

	UPROPERTY(EditAnywhere)
	float _movementSpeed;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
