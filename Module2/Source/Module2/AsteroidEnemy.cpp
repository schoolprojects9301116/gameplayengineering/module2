// Fill out your copyright notice in the Description page of Project Settings.


#include "AsteroidEnemy.h"
#include "AsteroidsGameMode.h"
#include "AsteroidsProjectile.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
AAsteroidEnemy::AAsteroidEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_collider = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	SetRootComponent(_collider);


	_asteroidMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AsteroidMeshComponent"));
	_asteroidMesh->SetupAttachment(_collider);

	_worldWrapComponent = CreateDefaultSubobject<UWorldWrapComponent>(TEXT("WorldWrapComponent"));
}

// Called when the game starts or when spawned
void AAsteroidEnemy::BeginPlay()
{
	Super::BeginPlay();
	auto gameMode = GetWorld()->GetAuthGameMode<AAsteroidsGameMode>();
	if (gameMode)
	{
		gameMode->ModifyAsteroidCount(1);
	}
	//_collider->OnComponentBeginOverlap.AddDynamic(this, &AAsteroidEnemy::OnOverlapBegin);
}

// Called every frame
void AAsteroidEnemy::Tick(float DeltaTime)
{
	SetActorScale3D(FVector(_scaleFactor * _asteroidSize));

	if (_collisionProtectionTimer > 0)
	{
		_collisionProtectionTimer -= DeltaTime;
		if (_collisionProtectionTimer <= 0)
		{
			_collider->OnComponentBeginOverlap.AddDynamic(this, &AAsteroidEnemy::OnOverlapBegin);
		}
	}
	Super::Tick(DeltaTime);

	FVector offset = GetActorForwardVector();
	offset *= DeltaTime * _asteroidSpeed / _asteroidSize;

	AddActorWorldOffset(offset);
}

float AAsteroidEnemy::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float scoreValue = _scoreValue / _asteroidSize;
	_collider->OnComponentBeginOverlap.Clear();
	if (Damage < _asteroidSize)
	{
		_asteroidSize -= static_cast<int>(Damage);
		UWorld* world = GetWorld();
		if (world)
		{
			FActorSpawnParameters spawnParams;
			spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
			FVector spawnCenterLocation = GetActorLocation();
			int numAsteroidsToSpawn = FMath::RandRange(_minAsteroidSpawnCount, _maxAsteroidSpawnCount);
			float maxArcSize = 360 / numAsteroidsToSpawn;
			float startPoint = FMath::RandRange(0.0f, 360.0f);
			for (int i = 0; i < numAsteroidsToSpawn; i++)
			{
				float spawnOrientation = startPoint + FMath::RandRange(0.0f, maxArcSize);
				if (spawnOrientation > 360)
				{
					spawnOrientation -= 360;
				}
				FRotator spawnRotator(0, spawnOrientation, 0);
				auto spawnPoint = spawnRotator.Vector() * 100;
				spawnPoint += spawnCenterLocation;
				auto newAsteroid = world->SpawnActor<AAsteroidEnemy>(_asteroidSpawnType, spawnPoint, spawnRotator, spawnParams);
				if (newAsteroid)
				{
					newAsteroid->_asteroidSize = _asteroidSize;
				}

				startPoint += maxArcSize;
			}
		}
	}
	auto gameMode = GetWorld()->GetAuthGameMode<AAsteroidsGameMode>();
	if (gameMode)
	{
		gameMode->ModifyAsteroidCount(-1);
		if (Cast<AAsteroidsProjectile>(DamageCauser) != nullptr)
		{
			gameMode->ModifyScore(scoreValue);
		}
	}
	UGameplayStatics::PlaySound2D(GetWorld(), _damageSound, _soundVolume, _soundPitch);
	Destroy();
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void AAsteroidEnemy::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
	, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	_collider->OnComponentBeginOverlap.Clear();
	_collisionProtectionTimer = _collisionProtectionTime;
	FDamageEvent damageEvent;
	TSubclassOf<UDamageType> dmgType = UDamageType::StaticClass();
	damageEvent.DamageTypeClass = dmgType;
	auto otherAsteroid = Cast<AAsteroidEnemy>(OtherActor);
	if (otherAsteroid == nullptr || otherAsteroid->_asteroidSize <= _asteroidSize)
	{
		OtherActor->TakeDamage(1, damageEvent, GetInstigatorController(), this);
	}
}

void AAsteroidEnemy::SetSpeed(float speed)
{
	_asteroidSpeed = speed;
}

float AAsteroidEnemy::GetSpeed() const
{
	return _asteroidSpeed;
}
