// Fill out your copyright notice in the Description page of Project Settings.

#include "AsteroidsCharacter.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "AsteroidsGameMode.h"

// Sets default values
AAsteroidsCharacter::AAsteroidsCharacter()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create Components
	CharacterMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CharacterMeshComponent"));
	CharacterMesh->SetGenerateOverlapEvents(false);
	_worldWrapComponent = CreateDefaultSubobject<UWorldWrapComponent>(TEXT("WorldWrapComponent"));

	// Attatch Components
	SetRootComponent(CharacterMesh);
}

// Called when the game starts or when spawned
void AAsteroidsCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	// Setup Variables

	_currentMovementSpeed = 0;
	_currentRotationSpeed = 0;
	_currentImmunityTime = _respawnImmunityTime;
	CharacterMesh->SetGenerateOverlapEvents(false);
	_usingPrimaryWeapon = true;
}

// Called every frame
void AAsteroidsCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (_currentImmunityTime > 0.0f)
	{
		_currentImmunityTime -= DeltaTime;
		if (_currentImmunityTime <= 0.0f)
		{
			CharacterMesh->SetGenerateOverlapEvents(true);
		}
	}

	AddActorWorldRotation(FRotator(0, _currentRotationSpeed * DeltaTime, 0));
	FVector offsetVector = GetActorForwardVector();
	offsetVector *= _currentMovementSpeed * DeltaTime;
	AddActorWorldOffset(offsetVector);

	_currentMovementSpeed -= _movementDrag * DeltaTime;
	_currentMovementSpeed = FMath::Clamp(_currentMovementSpeed, 0.0f, _maxMovementSpeed);
	if (_currentRotationSpeed < 0)
	{
		_currentRotationSpeed += _rotationDrag * DeltaTime;
		_currentRotationSpeed = FMath::Clamp(_currentRotationSpeed, -_maxRotationSpeed, 0.0f);
	}
	else
	{
		_currentRotationSpeed -= _rotationDrag * DeltaTime;
		_currentRotationSpeed = FMath::Clamp(_currentRotationSpeed, 0.0f, _maxRotationSpeed);
	}
}

// Called to bind functionality to input
void AAsteroidsCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("RotateLR"), this, &AAsteroidsCharacter::RotateLR);
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AAsteroidsCharacter::MoveForward);
	PlayerInputComponent->BindAction(TEXT("Fire"), EInputEvent::IE_Pressed, this, &AAsteroidsCharacter::FireBullet);
	PlayerInputComponent->BindAction(TEXT("SwitchWeapon"), EInputEvent::IE_Pressed, this, &AAsteroidsCharacter::SwitchWeapons);
}

void AAsteroidsCharacter::RotateLR(float inputValue)
{
	_currentRotationSpeed += inputValue * _rotationAcceleration;
	_currentRotationSpeed = FMath::Clamp(_currentRotationSpeed, -_maxRotationSpeed, _maxRotationSpeed);
}

void AAsteroidsCharacter::MoveForward(float inputValue)
{
	_currentMovementSpeed += inputValue * _movementAcceleration;
	_currentMovementSpeed = FMath::Clamp(_currentMovementSpeed, 0, _maxMovementSpeed);
}

void AAsteroidsCharacter::FireBullet()
{
	UWorld* world = GetWorld();
	if (world)
	{
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = GetInstigator();
		FVector spawnLocation = GetActorLocation();
		spawnLocation += GetActorForwardVector() * 300;
		FRotator spawnOrientation = GetActorRotation();

		world->SpawnActor<AAsteroidsProjectile>(
				_usingPrimaryWeapon ? _primaryProjectile : _secondaryProjectile
			, spawnLocation, spawnOrientation, spawnParams);
	}
}

float AAsteroidsCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	auto gameMode = GetWorld()->GetAuthGameMode<AAsteroidsGameMode>();
	if (gameMode)
	{
		gameMode->PlayerDied(this);
	}
	UGameplayStatics::PlaySound2D(GetWorld(), _deathSound, _soundVolume, _soundPitch);
	Destroy();
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void AAsteroidsCharacter::SwitchWeapons()
{
	_usingPrimaryWeapon = !_usingPrimaryWeapon;
}