// Fill out your copyright notice in the Description page of Project Settings.


#include "AsteroidsProjectile.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AAsteroidsProjectile::AAsteroidsProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_colliderComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	SetRootComponent(_colliderComponent);
	_colliderComponent->InitSphereRadius(5.0f);

	_projectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
	_projectileMesh->SetupAttachment(_colliderComponent);

	_worldWrapComponent = CreateDefaultSubobject<UWorldWrapComponent>(TEXT("WorldWrapComponent"));
}

// Called when the game starts or when spawned
void AAsteroidsProjectile::BeginPlay()
{
	Super::BeginPlay();
	_currentLifeTime = 0.0f;
	_colliderComponent->OnComponentBeginOverlap.AddDynamic(this, &AAsteroidsProjectile::OnOverlapBegin);
	UGameplayStatics::PlaySound2D(GetWorld(), _fireSound, _fireSoundVolume, _fireSoundPitch);
}

// Called every frame
void AAsteroidsProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector offsetVector = GetActorForwardVector();
	offsetVector *= _projectileSpeed * DeltaTime;

	AddActorWorldOffset(offsetVector);

	_currentLifeTime += DeltaTime;
	if (_currentLifeTime > _projectileLifeTime)
	{
		Destroy();
	}
}

void AAsteroidsProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
	, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	_colliderComponent->OnComponentBeginOverlap.Clear();
	if (OtherActor != this)
	{
		DamageTarget(OtherActor);
	}
	Destroy();
}
