// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AsteroidsProjectile.h"
#include "AsteroidsLaserProjectile.generated.h"

/**
 * 
 */
UCLASS()
class MODULE2_API AAsteroidsLaserProjectile : public AAsteroidsProjectile
{
	GENERATED_BODY()
	
protected:
	virtual void DamageTarget(AActor* OtherActor) override;
};
