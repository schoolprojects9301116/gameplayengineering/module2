// Fill out your copyright notice in the Description page of Project Settings.


#include "AsteroidManager.h"
#include "AsteroidsGameState.h"
#include "AsteroidsGameMode.h"

// Sets default values
AAsteroidManager::AAsteroidManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAsteroidManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAsteroidManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AAsteroidsGameState* gameState = GetWorld()->GetAuthGameMode()->GetGameState<AAsteroidsGameState>();
	if (gameState->AsteroidCount <= 0)
	{
		if (!_betweenRounds)
		{
			_betweenRounds = true;
			_timeUntilNextRound = TimeBetweenRounds;
			gameState->CurrentWave++;
		}
		else
		{
			_timeUntilNextRound -= DeltaTime;
			if (_timeUntilNextRound <= 0)
			{
				_betweenRounds = false;
				SpawnRound();
			}
		}
	}
}

void AAsteroidManager::SpawnRound()
{
	AAsteroidsGameState* gameState = GetWorld()->GetAuthGameMode()->GetGameState<AAsteroidsGameState>();
	int numAsteroidsToSpawn = AsteroidsPerRound;
	if (WaveSpawnMultiplier > 1)
	{
		numAsteroidsToSpawn *= WaveSpawnMultiplier * gameState->CurrentWave;
	}
	if (numAsteroidsToSpawn > MaxAsteroids)
	{
		numAsteroidsToSpawn = MaxAsteroids;
	}
	FActorSpawnParameters spawnParams;
	auto world = GetWorld();
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	for (int i = 0; i < numAsteroidsToSpawn; i++)
	{
		FRotator spawnOrientation(0, FMath::RandRange(0.0f, 360.f), 0);
		FVector spawnPos(FMath::RandRange(SpawnMinBounds.X, SpawnMaxBounds.X)
			, FMath::RandRange(SpawnMinBounds.Y, SpawnMaxBounds.Y), 160.f);
		auto asteroid = world->SpawnActor<AAsteroidEnemy>(_asteroidSpawnType, spawnPos, spawnOrientation, spawnParams);
		if (asteroid != nullptr && WaveSpeedMultiplier > 0)
		{
			asteroid->SetSpeed(asteroid->GetSpeed() * WaveSpeedMultiplier * gameState->CurrentWave);
		}
	}
}

