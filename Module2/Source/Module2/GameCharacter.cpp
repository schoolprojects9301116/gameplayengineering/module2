// Fill out your copyright notice in the Description page of Project Settings.


#include "GameCharacter.h"

// Sets default values
AGameCharacter::AGameCharacter()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_rootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("CharacterSceneComponent"));
	_CharacterMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CharacterMesh"));
	//SetRootComponent(_rootSceneComponent);
	_CharacterMesh->SetupAttachment(_rootSceneComponent);
	
}

// Called when the game starts or when spawned
void AGameCharacter::BeginPlay()
{
	Super::BeginPlay() ;
	
}

// Called every frame
void AGameCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGameCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("RotateLR"), this, &AGameCharacter::RotateLR);
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AGameCharacter::MoveForward);
}

void AGameCharacter::RotateLR(float inputValue)
{
	AddActorWorldRotation(FRotator(0, inputValue * _rotationSpeed, 0));
}

void AGameCharacter::MoveForward(float inputValue)
{
	FVector movementDirection = GetActorForwardVector();
	movementDirection *= inputValue * _movementSpeed;
	FVector newLocation = GetActorLocation() + movementDirection;
	SetActorLocation(newLocation);
}
