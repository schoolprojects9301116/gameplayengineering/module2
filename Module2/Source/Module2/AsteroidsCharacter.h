// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "AsteroidsProjectile.h"
#include "WorldWrapComponent.h"
#include "Sound/SoundBase.h"

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "AsteroidsCharacter.generated.h"

UCLASS()
class MODULE2_API AAsteroidsCharacter : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AAsteroidsCharacter();

	void RotateLR(float inputValue);
	void MoveForward(float inputValue);
	void FireBullet();
	void SwitchWeapons();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* CharacterMesh;

	UPROPERTY(EditAnywhere)
	float _rotationAcceleration;

	UPROPERTY(EditAnywhere)
	float _maxRotationSpeed;

	UPROPERTY(EditAnywhere)
	float _movementAcceleration;

	UPROPERTY(EditAnywhere)
	float _maxMovementSpeed;

	UPROPERTY(EditAnywhere)
	float _rotationDrag;

	UPROPERTY(EditAnywhere)
	float _movementDrag;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AAsteroidsProjectile> _primaryProjectile;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AAsteroidsProjectile> _secondaryProjectile;

	UPROPERTY(EditAnywhere)
	UWorldWrapComponent* _worldWrapComponent;

	UPROPERTY(EditAnywhere)
	float _respawnImmunityTime = 2.0f;

	UPROPERTY(EditAnywhere)
	USoundBase* _deathSound;

	UPROPERTY(EditAnywhere)
	float _soundVolume = 1.f;

	UPROPERTY(EditAnywhere)
	float _soundPitch = 1.f;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool _usingPrimaryWeapon;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

private:
	float _currentRotationSpeed;
	float _currentMovementSpeed;
	float _currentImmunityTime;
};
