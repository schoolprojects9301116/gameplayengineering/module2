// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "AsteroidsGameState.generated.h"

/**
 * 
 */
UCLASS()
class MODULE2_API AAsteroidsGameState : public AGameStateBase
{
	GENERATED_BODY()
public:

	AAsteroidsGameState();

	UPROPERTY(EditAnywhere)
	int StartingPlayerLives;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int CurrentWave;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int NumPlayerLives;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int PlayerScore;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int AsteroidCount;
};
