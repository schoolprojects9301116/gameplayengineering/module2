// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldWrapComponent.h"

// Sets default values for this component's properties
UWorldWrapComponent::UWorldWrapComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWorldWrapComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}


// Called every frame
void UWorldWrapComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	AActor* myOwner = GetOwner();
	if (myOwner)
	{
		FVector currentLocation = myOwner->GetActorLocation();
		bool updateLocation = false;
		if (currentLocation.X < _xMinWrapPos)
		{
			currentLocation.X = _xMaxWrapPos - _xWrapOffset;
			updateLocation = true;
		}
		else if (currentLocation.X > _xMaxWrapPos)
		{
			currentLocation.X = _xMinWrapPos + _xWrapOffset;
			updateLocation = true;
		}

		if (currentLocation.Y < _yMinWrapPos)
		{
			currentLocation.Y = _yMaxWrapPos - _yWrapOffset;
			updateLocation = true;
		}
		else if (currentLocation.Y > _yMaxWrapPos)
		{
			currentLocation.Y = _yMinWrapPos + _yWrapOffset;
			updateLocation = true;
		}

		if (updateLocation)
		{
			myOwner->SetActorLocation(currentLocation);
		}
	}
}

