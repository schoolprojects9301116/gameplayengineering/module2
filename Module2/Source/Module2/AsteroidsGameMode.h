// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AsteroidsGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MODULE2_API AAsteroidsGameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	AAsteroidsGameMode();

	void PlayerDied(APawn* player);
	void ModifyAsteroidCount(int modifier);
	void ModifyScore(int modifier);

	virtual void StartPlay();
};
