// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SphereComponent.h"
#include "WorldWrapComponent.h"
#include "Sound/SoundBase.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AsteroidEnemy.generated.h"

UCLASS()
class MODULE2_API AAsteroidEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAsteroidEnemy();

	void SetSpeed(float speed);
	float GetSpeed() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	USphereComponent* _collider;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* _asteroidMesh;

	UPROPERTY(EditAnywhere)
	UWorldWrapComponent* _worldWrapComponent;

	UPROPERTY(EditAnywhere)
	int _asteroidSize;

	UPROPERTY(EditAnywhere)
	int _minAsteroidSpawnCount;

	UPROPERTY(EditAnywhere)
	int _maxAsteroidSpawnCount;

	UPROPERTY(EditAnywhere)
	float _asteroidSpeed;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AAsteroidEnemy> _asteroidSpawnType;

	UPROPERTY(EditAnywhere)
	float _scaleFactor = 1.25;

	UPROPERTY(EditAnywhere)
	int _scoreValue = 30;

	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UPROPERTY(EditAnywhere)
	float _collisionProtectionTime = 0.5f;

	UPROPERTY(EditAnywhere)
	float _collisionProtectionTimer = 0.5f;

	UPROPERTY(EditAnywhere)
	USoundBase* _damageSound;

	UPROPERTY(EditAnywhere)
	float _soundVolume = 1.f;

	UPROPERTY(EditAnywhere)
	float _soundPitch = 1.f;

public:	
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
		, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
