// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Module2GameModeBase.generated.h"


/**
 * 
 */
UCLASS()
class MODULE2_API AModule2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AModule2GameModeBase();
};
