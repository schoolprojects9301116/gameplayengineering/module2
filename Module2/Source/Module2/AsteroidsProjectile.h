// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "WorldWrapComponent.h"
#include "Sound/SoundBase.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AsteroidsProjectile.generated.h"

UCLASS()
class MODULE2_API AAsteroidsProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAsteroidsProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	USphereComponent* _colliderComponent;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* _projectileMesh;

	UPROPERTY(EditAnywhere)
	float _projectileSpeed;

	UPROPERTY(EditAnywhere)
	float _projectileLifeTime;

	UPROPERTY(EditAnywhere)
	UWorldWrapComponent* _worldWrapComponent;

	UPROPERTY(EditAnywhere)
	USoundBase* _fireSound;

	UPROPERTY(EditAnywhere)
	float _fireSoundVolume = 1.f;

UPROPERTY(EditAnywhere)
	float _fireSoundPitch = 1.f;

	virtual void DamageTarget(AActor* OtherActor) PURE_VIRTUAL(AAsteroidsProjectile::DamageTarget, );

private:
	float _currentLifeTime;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
		, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
