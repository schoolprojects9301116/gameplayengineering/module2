// Fill out your copyright notice in the Description page of Project Settings.


#include "AsteroidsMissileProjectile.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Particles/ParticleSystem.h"

void AAsteroidsMissileProjectile::DamageTarget(AActor* OtherActor)
{
	FDamageEvent damageEvent;
	TSubclassOf<UDamageType> dmgType = UDamageType::StaticClass();
	damageEvent.DamageTypeClass = dmgType;
	TArray<AActor*> ignoreActors;
	UGameplayStatics::ApplyRadialDamage(GetWorld(), 2, GetActorLocation(), _explosionRadius
		, dmgType, ignoreActors, this);
	UGameplayStatics::PlaySound2D(GetWorld(), _explosionSound, _explosionSoundVolume, _explosionSoundPitch);

	if (_explosionEffect)
	{
		auto particleSystem = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), _explosionEffect, GetActorLocation());
		particleSystem->SetWorldScale3D(FVector(3));
	}
}