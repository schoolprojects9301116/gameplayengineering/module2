// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AsteroidsProjectile.h"
#include "AsteroidsMissileProjectile.generated.h"

class UNiagaraSystem;
class UParticleSystem;

/**
 * 
 */
UCLASS()
class MODULE2_API AAsteroidsMissileProjectile : public AAsteroidsProjectile
{
	GENERATED_BODY()

protected:

	virtual void DamageTarget(AActor* OtherActor) override;

	UPROPERTY(EditAnywhere)
	float _explosionRadius;

	UPROPERTY(EditAnywhere)
	USoundBase* _explosionSound;

	UPROPERTY(EditAnywhere)
	float _explosionSoundVolume = 1.f;

	UPROPERTY(EditAnywhere)
	float _explosionSoundPitch = 1.f;

	//UPROPERTY(EditAnywhere)
	//UNiagaraSystem * _explosionEffect;
	UPROPERTY(EditAnywhere)
	UParticleSystem* _explosionEffect;
};
