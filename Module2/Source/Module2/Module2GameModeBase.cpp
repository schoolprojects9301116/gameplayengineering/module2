// Copyright Epic Games, Inc. All Rights Reserved.

#include "Module2GameModeBase.h"
#include "GameCharacter.h"


AModule2GameModeBase::AModule2GameModeBase()
{
	DefaultPawnClass = AGameCharacter::StaticClass();
}