// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WorldWrapComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MODULE2_API UWorldWrapComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWorldWrapComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	float _xMinWrapPos;

	UPROPERTY(EditAnywhere)
	float _xMaxWrapPos;

	UPROPERTY(EditAnywhere)
	float _yMinWrapPos;

	UPROPERTY(EditAnywhere)
	float _yMaxWrapPos;

	UPROPERTY(EditAnywhere)
	float _xWrapOffset;

	UPROPERTY(EditAnywhere)
	float _yWrapOffset;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
