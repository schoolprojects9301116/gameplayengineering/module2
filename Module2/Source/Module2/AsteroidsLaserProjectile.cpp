// Fill out your copyright notice in the Description page of Project Settings.


#include "AsteroidsLaserProjectile.h"

void AAsteroidsLaserProjectile::DamageTarget(AActor* OtherActor)
{
	FDamageEvent damageEvent;
	TSubclassOf<UDamageType> dmgType = UDamageType::StaticClass();
	damageEvent.DamageTypeClass = dmgType;
	OtherActor->TakeDamage(1, damageEvent, GetInstigatorController(), this);
}