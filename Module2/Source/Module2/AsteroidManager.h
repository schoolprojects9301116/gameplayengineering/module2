// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AsteroidEnemy.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AsteroidManager.generated.h"

UCLASS()
class MODULE2_API AAsteroidManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAsteroidManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:
	void SpawnRound();

	UPROPERTY(EditAnywhere)
		int AsteroidsPerRound = 2;

	UPROPERTY(EditAnywhere)
		int MaxAsteroids = 10;

	UPROPERTY(EditAnywhere)
		float WaveSpawnMultiplier = 1.5;

	UPROPERTY(EditAnywhere)
		float WaveSpeedMultiplier = 0.8;

	UPROPERTY(EditAnywhere)
		float TimeBetweenRounds = 2.f;

	UPROPERTY(EditAnywhere)
		FVector2D SpawnMinBounds;

	UPROPERTY(EditAnywhere)
		FVector2D SpawnMaxBounds;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class AAsteroidEnemy> _asteroidSpawnType;

	bool _betweenRounds = false;
	float _timeUntilNextRound = 0.0f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
